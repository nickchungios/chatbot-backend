import nltk
import numpy as np
from nltk.stem.porter import PorterStemmer

nltk.data.path.append("/tmp")

try:
  nltk.data.find('tokenizers/punkt')
except LookupError:
  nltk.download("punkt", download_dir = "/tmp")

stemmer = PorterStemmer()

def tokenize(sentence):
  return nltk.word_tokenize(sentence)

def stem(word):
  return stemmer.stem(word.lower())

def bag_of_words(tokenized_sentence, all_words):
  """
  sentence = ["hello", "how", "are", "you"]
  words = ["hi", "hello", "I", "you", "bye", "thank", "cool"]
  bog   = [  0,    1,      0,    1,    0,      0,      0]
  """
  tokenized_sentence = [stem(w) for w in tokenized_sentence]

  bag = np.zeros(len(all_words), dtype=np.float32)
  for idx, w in enumerate(all_words):
    if w in tokenized_sentence:
      bag[idx] = 1.0

  return bag


# EXAMPLE
# words = ['Organize', 'organizes', 'organizing']
# stemm_words = [stem(w) for w in words]
# print(stemm_words)

# sentence = ["hello", "how", "are", "you"]
# words = ["hi", "hello", "I", "you", "bye", "thank", "cool"]
# bog = bag_of_words(sentence, words)
# print(bog)