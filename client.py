import socketio

sio = socketio.Client()

@sio.event
def connect():
    print('connected to server')
    sentence = input('You: ')
    sio.emit('aaa', sentence)

@sio.event
def disconnect():
    print('disconnected from server')

@sio.on('aaa_response')
def res(res):
    print(f"Nicky: {res}")
    sentence = input('You: ')
    sio.emit('aaa', sentence)

sio.connect('http://localhost:8000')
sio.wait()
