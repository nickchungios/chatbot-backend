FROM public.ecr.aws/lambda/python:3.7

# Install Packages
COPY requirements.txt ${LAMBDA_TASK_ROOT}
RUN pip3 install -r requirements.txt

# Main Script
ADD src/* ${LAMBDA_TASK_ROOT}
CMD [ "lambda.answer" ]

########### LOCAL ###########
# FROM python:3.7.2-slim

# WORKDIR /app
# ADD . /app

# # Install Packages
# RUN pip3 install -r requirements.txt

# # Main Script
# CMD python3 server.py